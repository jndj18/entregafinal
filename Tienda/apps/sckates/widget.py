from django.forms import widgets
from django.utils.safestring import mark_safe
from django.shortcuts import render

class ImageWidget(widgets.FileInput):

    def __init__(self, attrs={}):
        super(ImageWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None):
        output = []
        if value and hasattr(value, "url"):
            output.append('<img class="img-fluid profile-picture" src="/static/%s" alt="/static/%s">' % (value.url, value))
        output.append('<input type="file" class="form-control" name="%s">' % (name))
        return mark_safe(u''.join(output))