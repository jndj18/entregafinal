from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser


# Create your models here.


class Usuario(AbstractUser):
    pass
    image = models.ImageField(null=True, upload_to="images/usuarios")


    def __str__(self):
        return self.username
    
    def getImageUrl(self):
        #images/usuarios/Imagen_de_usuario.jpg
        return "static/" + str(self.image)
