"""Tienda URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from . import views
from rest_framework import routers

app_name = "sckates"

router = routers.DefaultRouter()
router.register(r'api/users', views.UsuariosViewSet)

urlpatterns = [
    

    url(r'^$', views.home, name="home"),
    url(r'^listar-clientes$', views.listar_clientes, name="cliente.listar"),
    path('editar-cliente/<int:idCliente>', views.editar_cliente, name="cliente.editar"),
    path('borrar-cliente/<int:idCliente>', views.borrar_cliente, name="cliente.borrar"),
    url(r'^agregar-cliente$', views.agregar_cliente, name="cliente.agregar"),
    
    url(r'^logout$', views.logout_account, name="logout"),
    url(r'^login$', views.login_account, name="login"),
    url(r'^', include(router.urls)),
    url(r'^ruedas', views.ruedas, name="ruedas"),
    url(r'^tablas', views.tablas, name="tablas"),
    url(r'^trucks', views.trucks, name="trucks"),

]

