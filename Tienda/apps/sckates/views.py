from django.shortcuts import render, redirect
from .forms import *
from .models import *
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

from django.contrib.auth.decorators import login_required, user_passes_test
from rest_framework import viewsets
from .serializers import UsuarioSerializer

def home(request):
    return render(request, 'home.html')

def ruedas(request):
    return render(request, 'paginas/ruedas/rueda1.html')

def tablas(request):
    return render(request, 'paginas/tablas/tabla1.html')

def trucks(request):
    return render(request, 'paginas/trucks/truck.html')



def agregar_cliente(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST, request.FILES or None)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('sckates:cliente.listar')
    else:
        form = SignUpForm()

    return render(request, 'cliente/form-cliente.html', {'form':form})

@login_required
def listar_clientes(request):
    users = Usuario.objects.all()
    return render(request, "cliente/listar_clientes.html", {'clientes': users})

@login_required
def editar_cliente(request, idCliente):
    user = Usuario.objects.get(id=idCliente)
    form = SignUpForm(request.POST or None, request.FILES or None, instance=user) #CUANDO PASAS ALGUN ARCHIVO DEBE SER ASI (request.POST or None, request.FILES or None, instance=instance)

    if form.is_valid():
        form.save()
        return redirect('sckates:cliente.listar')

    return render(request, "cliente/form-cliente.html",{'form':form})   

@user_passes_test(lambda u: u.is_superuser)
def borrar_cliente(request, idCliente):    
    user= Usuario.objects.get(id=idCliente)
    user.delete()
    return redirect('sckates:cliente.listar')   
 
@login_required 
def logout_account(request):
    logout(request)
    return redirect('sckates:home')

def login_account(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        
        user = authenticate(username=username, password=password)
        if user and user.is_active:
            login(request, user)
            return redirect('sckates:cliente.listar')

    return render(request, 'login.html')

class UsuariosViewSet(viewsets.ModelViewSet):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer