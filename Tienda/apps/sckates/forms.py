from django import forms
from .views import *
from .models import *
from .widget import ImageWidget

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User



class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30, required=True, help_text='Obligatorio. Ingresa tu nombre de usuario', label="Nombre de usuario")
    first_name = forms.CharField(max_length=30, required=True, help_text='Obligatorio. Ingresa tu nombre.', label="Nombre")
    last_name = forms.CharField(max_length=30, required=True, help_text='Obligatorio. Ingresa tu(s) apellido(s)', label="Apellido(s)")
    email = forms.EmailField(max_length=254, required=True, help_text='Obligatorio. Ingresa tu dirección de correo electrónico', label="Correo electrónico")
    password1 = forms.CharField(max_length=30, required=True, help_text='Obligatorio. Ingresa tu contraseña, debe ser una contraseña segura.', label="Contraseña")
    password2 = forms.CharField(max_length=30, required=True, help_text='Obligatorio. Re-ingresa tu contraseña, esta debe ser identica al campo anterior.', label="Confirmar contraseña")
    image = forms.ImageField(required=True, label="Foto actual:")

    username.widget = forms.TextInput(attrs={'class':'form-control'})
    first_name.widget = forms.TextInput(attrs={'class':'form-control'})
    last_name.widget = forms.TextInput(attrs={'class':'form-control'})
    email.widget = forms.TextInput(attrs={'class':'form-control'})
    password1.widget = forms.PasswordInput(attrs={'class':'form-control'})
    password2.widget = forms.PasswordInput(attrs={'class':'form-control'})
    image.widget = ImageWidget()
    class Meta:
        model = Usuario
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'image')




