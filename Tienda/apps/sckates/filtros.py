from .models import *
import django_filters

class UsuarioFilter(django_filters.FilterSet):
    class Meta:
        model = Usuario
        fields = ['',]
        labels = {''}
        