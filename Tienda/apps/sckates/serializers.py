from rest_framework import serializers
from .models import *


class UsuarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Usuario
        fields = ('username','first_name','last_name','email', 'image')
